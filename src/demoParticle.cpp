#include "demoParticle.h"
float R, G, B;
//------------------------------------------------------------------
demoParticle::demoParticle(){
	attractPoints = NULL;
}

//------------------------------------------------------------------
void demoParticle::setMode(particleMode newMode){
	mode = newMode;
}

//------------------------------------------------------------------
void demoParticle::setAttractPoints( vector <ofPoint> * attract ){
	attractPoints = attract;
}

//------------------------------------------------------------------
void demoParticle::reset(){
	//the unique val allows us to set properties slightly differently for each particle
	uniqueVal = ofRandom(-10000000, 1000000);
	
	

	pos.x = ofRandomWidth();
	pos.y = ofRandomHeight();
	
	vel.x = ofRandom(-0.1, 0.1);//Velocity X
	vel.y = ofRandom(-0.1, 0.1);//Velocity y -- the distance between the values determines the speed upon reset.
	
	frc   = ofPoint(0,0,0); //Force value
	
	scale = ofRandom(1.0, 0.1);// Changes the scale upon reset.
	
	if( mode == PARTICLE_MODE_NOISE ){
		drag  = ofRandom(0.97, 0.99);// The pulling force orn drag of the objects.
		vel.y = fabs(vel.y) * 0.1; //make the particles all be going down
	}else{
		drag  = ofRandom(0.95, 0.998);	
	}
}

//------------------------------------------------------------------
void demoParticle::update(){

	
	
	if( mode == PARTICLE_MODE_ATTRACT ){
		ofPoint attractPt(ofGetScreenWidth()/2, ofGetScreenHeight()/2);
	//ofPoint attractPt(ofGetMouseX(), ofGetMouseY());
		frc = attractPt-pos; // we get the attraction force/vector by looking at the mouse pos relative to our pos
		frc.normalize(); //by normalizing we disregard how close the particle is to the attraction point 
		
		vel *= drag; //apply drag
		vel += frc * 5; //apply force-- can scale the value of force
	}
	
	
	
	//position changes dependant on velocity
	pos += vel; 
	
	//Constrains the particles to just the screen
	if( pos.x > ofGetWidth() ){
		pos.x = ofGetWidth();
		vel.x *= -1.0;
	}else if( pos.x < 0 ){
		pos.x = 0;
		vel.x *= -1.0;
	}
	if( pos.y > ofGetHeight() ){
		pos.y = ofGetHeight();
		vel.y *= -1.0;
	}
	else if( pos.y < 0 ){
		pos.y = 0;
		vel.y *= -1.0;
	}	
		
}

//------------------------------------------------------------------
void demoParticle::draw(){

	if( mode == PARTICLE_MODE_ATTRACT ){

		//Sets RGB random values of particles via float RGB
		R = ofRandom(0, 200);
		G = ofRandom(0, 200);
		B = ofRandom(0, 200); 
		
		
		ofSetColor(R, G, B);
		ofFill();
		

		
	}
	
			
	ofDrawCircle(pos.x, pos.y, scale * 30.0);
}

